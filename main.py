from flask import Flask
from routes.api import api

app = Flask(__name__)
app.register_blueprint(api)

@app.errorhandler(404)
def not_found(err):
    return "Not Found"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8083')
