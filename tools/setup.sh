# TODO: Virtualenv is through pip
# TODO: Nginx and uWSGI configuration symbolic linking
# TODO: Maybe a single installer for all the components (Nginx and uWSGI)

# Setup absolute path
THIS_PATH=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

# Check for dependencies first before doing setup
DEPENDS=($(cat ${THIS_PATH}/depends.txt))

for depend in "${DEPENDS[@]}"
do
	echo "Checking for ${depend} ..."
	HAS_DEP=$(which ${depend})
	if [ -z "$HAS_DEP" ]; then
		echo "[!] Please install ${depend} first"
		return
	else
		echo "[/] Already Installed"
	fi
done

# Setup Virtual Environment
echo "Setting up Virtual Environment ..."
if [ -d "${THIS_PATH}/app-env/" ]; then
	echo "[/] Already Done"
else
	virtualenv ${THIS_PATH}/app-env/
	echo "... Done"
fi

# Enter Virtual Environment
echo "Entering Virtual Environment ..."
source ${THIS_PATH}/app-env/bin/activate
echo "... Done"

# Install modules
echo "Installing Python Dependencies ..."
MODULES=($(cat ${THIS_PATH}/modules.txt))

for module in "${MODULES[@]}"
do
	echo "... Installing: ${module}"
	CHECK=$(pip show ${module})
	
	if [ -z "$CHECK" ]; then
		pip install ${module}
		echo "...... Done"
	else
		echo "... [/] Already Installed"
	fi
done

