from flask import Flask
from flask import render_template
from flask import Blueprint

api = Blueprint('main_api', __name__, template_folder='../templates', static_folder='../static') 

@api.route("/")
def dashboard():
    return render_template('index.html')

